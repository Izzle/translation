0.1.2
-----
- Addded readme file

0.1.1
-----
- Added phpunit tests

0.1.0
-----
- Added replacing parameters in translation strings
- Added key prefix
- Removed autoloader and debug stuff
- Removed default parameter

0.0.1
-----
- Added basic functions
